<?php

$webhookUrl = 'https://hooks.slack.com/services/XXXXXXXXX/XXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX';


$message = [
  "attachments" => [
    [
      "color" => "#36a64f",
      "title" => "Slack API Documentation",
      "title_link" => "https://api.slack.com/",
      "text" => "Optional text that appears within the attachment",
      "fields" => [
        [
          "title" => "Priority",
          "value" => "High",
          "short" => true
        ],
        [
          "title" => "Priority",
          "value" => "High",
          "short" => true
        ],
        [
          "title" => "Priority",
          "value" => "High",
          "short" => false
        ]
      ]
    ]
  ]
];

$message = 'payload=' . urlencode(json_encode($message));
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $webhookUrl);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
curl_exec($ch);
curl_close($ch);
